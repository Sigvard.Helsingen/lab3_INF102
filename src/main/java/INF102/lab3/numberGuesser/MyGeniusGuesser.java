package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {
    private int lowerBound = -1;
    private int upperBound = -1;
    @Override
    public int findNumber(RandomNumber number) {

        if (lowerBound == -1){
            lowerBound = number.getLowerbound();
            upperBound = number.getUpperbound();

        }
        int mid = (lowerBound + upperBound)/2;
        int makeGuess = number.guess(mid);
        if (makeGuess == 0) {
            upperBound = -1;
            lowerBound = -1;
            return mid;
        } else if (makeGuess > 0) {
            upperBound = mid -1;
        } else if (makeGuess < 0) {
            lowerBound = mid +1;
        }

        return findNumber(number);
    }
}


//Kanskje lage ny metode som tar inn start slutt og number?

