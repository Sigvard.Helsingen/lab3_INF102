package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        if (list.size() <1) {
            return 0;
        }
        else {
            return list.get(0) + sum(list.subList(1,list.size()));
        }
    }
}


