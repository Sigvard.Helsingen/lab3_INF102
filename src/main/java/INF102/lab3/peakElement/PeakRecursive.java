package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        int n = numbers.size();
        if (n == 1)
            return numbers.get(0);

        if (numbers.get(0) > numbers.get(1))
            return numbers.get(0);

        if (numbers.get(n-1) > numbers.get(n-2))
            return numbers.get(n-1);
        
        numbers.remove(0);
        numbers.remove(n-2);
        return peakElement(numbers);
    }

}
